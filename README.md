# Goga Cafe

This is the source code and assets for Goga Boba, a casual cozy game made with the Godot engine and based on classical genetics. Tend a garden and breed Goga plants to harvest fruit for your boba cafe!

Play the game on [itch.io](https://jetpackgone.itch.io/gogo-boba)!

An entry for [Go Godot Jam 3](https://itch.io/jam/go-godot-jam-3/rate/1553865).

## Assets

### Art

All sprites were made by Gotodo for this game.

### Audio

- [On the Farm](https://ludoloonstudio.itch.io/on-the-farm) by LudoLoon Studio
- [SoupTonic's sound SFX Pack 1 - UI menu sounds](https://souptonic.itch.io/souptonic-sfx-pack-1-ui-sounds) by SoupTonic
- [Restaurant Ambiance](https://soundbible.com/1664-Restaurant-Ambiance.html) by stephan
- [Nature Ambiance](https://soundbible.com/1263-Nature-Ambiance.html) by nille
- [40 CC0 water / splash / slime SFX](https://opengameart.org/content/40-cc0-water-splash-slime-sfx) by rubberduck

### Font

[Give Away](https://www.dafont.com/give-away.font?l[]=10&l[]=1) by Syafrizal a.k.a. Khurasan

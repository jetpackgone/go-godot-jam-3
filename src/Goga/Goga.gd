extends StaticBody2D

onready var fruit_position = $FruitPosition
onready var growth_timer = $GrowthTimer
onready var sprite = $Sprite
onready var bee = $Bee
onready var bee_anim_player = $Bee/AnimationPlayer
onready var sparkles = $SparkleParticles

export(int) var garden_index = 0

# 0 - seed
# 1 - sprout
# 2 - flower
# 3 - fruit
export(int) var growth_stage = 0
export(Dictionary) var crossbreed_genes = {}
export(bool) var has_fruit = false
export(bool) var watered = false

# Dictionary of arrays of int
# Each entry is an int range 0 to 3, where each bit represents alleles
# 0 is recessive, 1 is dominant
# 00 (0) -> aa
# 01 (1) -> aA
# 10 (2) -> Aa
# 11 (3) -> AA
export(Dictionary) var genes = {
	"color": 1,
	"shape": 1,
	"taste": 1
}
var phenotypes: Dictionary
var shader_offset: float

# Called when the node enters the scene tree for the first time.
func _ready():
	sprite.texture = load(Constants.PLANT_SEED_TEXTURE)
	phenotypes = GeneHelper.get_phenotypes(self.genes)
	$Label.visible = DevMode.enabled
	$Label.text = str(growth_stage)
	var rng := RandomNumberGenerator.new()
	rng.randomize()
	shader_offset = rng.randf_range(0, 4)
	$Sprite.material = load("res://Assets/Shaders/wind_shader_material.tres").duplicate()
	$Sprite.material.set_shader_param("offset", shader_offset)
	EventBus.connect("dev_mode_toggled", self, "_on_dev_mode_toggled")

func _on_dev_mode_toggled(enabled):
	$Label.visible = enabled

func _on_GrowthTimer_timeout():
	pass_time()

func load_data(data):
	garden_index = data.garden_index
	growth_stage = data.growth_stage
	genes = data.genes
	has_fruit = data.has_fruit
	match growth_stage:
		Constants.GROWTH_STAGE.SEED:
			sprite.texture = load(Constants.PLANT_SEED_TEXTURE)
		Constants.GROWTH_STAGE.SPROUT:
			sprite.texture = load(Constants.PLANT_SPROUT_TEXTURE)
		Constants.GROWTH_STAGE.ADULT:
			sprite.texture = load(Constants.PLANT_ADULT_TEXTURE)
		Constants.GROWTH_STAGE.FLOWER:
			sprite.texture = load(Constants.PLANT_FLOWER_TEXTURE)
		Constants.GROWTH_STAGE.FRUIT:
			if has_fruit:
				_add_fruit(self.genes)
			sprite.texture = load(Constants.PLANT_ADULT_TEXTURE)

func get_data() -> Dictionary:
	var data = {}
	var parent = get_parent().get_parent() # Get plot index in grid
	data.garden_index = parent.get_index()
	data.growth_stage = growth_stage
	data.genes = genes
	data.has_fruit = has_fruit
	var fruit = get_fruit()
	if fruit == null:
		data.fruit_genes = {}
	else:
		data.fruit_genes = fruit.genes
	return data

func harvest():
	var fruit = get_fruit()
	EventBus.emit_signal("harvested", fruit.genes, self.genes, crossbreed_genes)
	fruit.queue_free()
	has_fruit = false
	sparkles.emitting = false

func pollinate(parent_genes: Dictionary):
	crossbreed_genes = parent_genes
	bee_anim_player.play("RESET")
	bee.show()
	bee_anim_player.play("pollinate")

func grow_fruit():
	var other_genes = crossbreed_genes
	if other_genes.empty():
		other_genes = self.genes
	var new_genes = GeneHelper.breed(self.genes, other_genes)
	_add_fruit(new_genes)
	crossbreed_genes = {}

func pass_time():
	match growth_stage:
		Constants.GROWTH_STAGE.SEED:
			growth_stage = Constants.GROWTH_STAGE.SPROUT
			sprite.texture = load(Constants.PLANT_SPROUT_TEXTURE)
		Constants.GROWTH_STAGE.SPROUT:
			growth_stage = Constants.GROWTH_STAGE.ADULT
			sprite.texture = load(Constants.PLANT_ADULT_TEXTURE)
		Constants.GROWTH_STAGE.ADULT:
			growth_stage = Constants.GROWTH_STAGE.FLOWER
			sprite.texture = load(Constants.PLANT_FLOWER_TEXTURE)
		Constants.GROWTH_STAGE.FLOWER:
			growth_stage = Constants.GROWTH_STAGE.FRUIT
			sprite.texture = load(Constants.PLANT_ADULT_TEXTURE)
			bee.hide()
			bee_anim_player.stop()
			grow_fruit()
			if GameStateManager.tutorial_index == 2:
				EventBus.emit_signal("hint_triggered", Constants.tutorial_hints[GameStateManager.tutorial_index])
				GameStateManager.tutorial_index += 1
		Constants.GROWTH_STAGE.FRUIT:
			if !has_fruit:
				growth_stage = Constants.GROWTH_STAGE.FLOWER
				sprite.texture = load(Constants.PLANT_FLOWER_TEXTURE)
	$Label.text = str(growth_stage)

func _add_fruit(new_genes: Dictionary):
	var fruit = load(Constants.PATH_FRUIT).instance()
	fruit.material = load("res://Assets/Shaders/fruit_wind_shader_material.tres").duplicate()
	fruit.material.set_shader_param("offset", shader_offset)
	fruit.genes = new_genes
	fruit_position.add_child(fruit)
	has_fruit = true
	sparkles.emitting = true

func get_fruit():
	if has_fruit && fruit_position.get_child_count() > 0:
		return fruit_position.get_child(0)
	return null

func show_scythe(display: bool):
	$Trash.visible = display

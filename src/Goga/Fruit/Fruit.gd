extends Sprite

var genes := {}

var PURPLE := '#d61af7'
var ORANGE := '#f77c1a'

var SMOOTH_YELLOW = "res://Goga/Fruit/smooth_yellow_fruit.tres"
var SMOOTH_RED = "res://Goga/Fruit/smooth_red_fruit.tres"
var SPIKY_YELLOW = "res://Goga/Fruit/spiky_yellow_fruit.tres"
var SPIKY_RED = "res://Goga/Fruit/spiky_red_fruit.tres"

func _ready():
	var phenotypes = GeneHelper.get_phenotypes(self.genes)
	if phenotypes[Constants.GENE_COLOR] == "red":
		if phenotypes[Constants.GENE_SHAPE] == "spiky":
			texture = load(SPIKY_RED)
		else:
			texture = load(SMOOTH_RED)
	else:
		if phenotypes[Constants.GENE_SHAPE] == "spiky":
			texture = load(SPIKY_YELLOW)
		else:
			texture = load(SMOOTH_YELLOW)

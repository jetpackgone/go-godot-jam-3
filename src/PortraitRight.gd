extends TextureRect

signal show_finished
signal hide_finished

onready var anim_player = $AnimationPlayer
onready var request_label = $P/Label
onready var timer = $Timer

var shown := false
var rng: RandomNumberGenerator
var portraits
var colors = ["red", "yellow"]
var shapes = ["smooth", "spiky"]
var requested_color = ""
var requested_shape = ""

func _ready():
	rng = RandomNumberGenerator.new()

func add_floating_num(value: int):
	var floating_num = load(Constants.FLOATING_NUMBER).instance()
	$NumberPosition2D.add_child(floating_num)
	floating_num.display(value)

func update_reaction(color: String, shape: String, taste: String):
	if taste == "sweet":
		if requested_color == color && requested_shape == shape:
			set_reaction_text(Constants.perfect_reactions)
			set_portrait("happy")
			GameStateManager.add_reputation(Constants.REPUTATION_PERFECT)
			add_floating_num(Constants.REPUTATION_PERFECT)
		elif requested_color != color:
			set_reaction_text(Constants.wrong_color_reactions)
			set_portrait("unhappy")
			GameStateManager.add_reputation(Constants.REPUTATION_OK)
			add_floating_num(Constants.REPUTATION_OK)
		else:
			set_reaction_text(Constants.wrong_shape_reactions)
			set_portrait("unhappy")
			GameStateManager.add_reputation(Constants.REPUTATION_OK)
			add_floating_num(Constants.REPUTATION_OK)
	else:
		set_reaction_text(Constants.bitter_reactions)
		set_portrait("disgusted")
		GameStateManager.add_reputation(Constants.REPUTATION_FAIL)
		add_floating_num(Constants.REPUTATION_FAIL)

func set_portrait(emotion: String):
	texture = load(portraits[emotion])

func show_random_portrait():
	generate_random_request()
	rng.randomize()
	var portrait_index = rng.randi_range(0, Constants.portraits.size() - 1)
	portraits = Constants.portraits[Constants.portraits.keys()[portrait_index]]
	texture = load(portraits["neutral"])
	anim_player.play("show")
	yield(anim_player, "animation_finished")
	emit_signal("show_finished")
	shown = true

func hide_portrait():
	anim_player.play("hide")
	yield(anim_player, "animation_finished")
	emit_signal("hide_finished")
	shown = false

func generate_random_request():
	rng.randomize()
	var color_index = rng.randi_range(0, colors.size() - 1)
	requested_color = colors[color_index]
	rng.randomize()
	var shape_index = rng.randi_range(0, shapes.size() - 1)
	requested_shape = shapes[shape_index]
	rng.randomize()
	var index = rng.randi_range(0, 2)
	match index:
		0:
			request_label.text = "I'll have a " + requested_shape.to_upper() + " " + requested_color.to_upper() + " boba, please."
		1:
			request_label.text = "May I have a " + requested_shape.to_upper() + " " + requested_color.to_upper() + " boba?"
		2:
			request_label.text = requested_shape.to_upper() + " " + requested_color.to_upper() + " boba for me."

func set_reaction_text(reactions: Array):
	rng.randomize()
	var index = rng.randi_range(0, reactions.size() - 1)
	request_label.text = reactions[index]

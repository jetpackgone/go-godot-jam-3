extends MarginContainer

signal dialogue_finished

onready var label = $P/M/Label
onready var timer = $Timer
onready var indicator = $P/Position2D/Indicator
onready var indicator_player = $P/Position2D/Indicator/AnimationPlayer
onready var menu_player = $MenuAnimationPlayer

export(float) var text_speed = 0.03

var dialog = []

var phrase_num = 0
var finished = false

# Called when the node enters the scene tree for the first time.
func _ready():
	timer.wait_time = text_speed
	indicator.visible = false

func _input(event):
	if event.is_action_pressed("ui_accept"):
		if finished:
			next_phrase()
		else:
			label.visible_characters = len(label.text)

func start():
	indicator.visible = false
	indicator_player.play("hover")
	menu_player.show_menu(self)
	next_phrase()

func next_phrase():
	indicator.visible = false
	if phrase_num >= len(dialog):
		close()
		return
	finished = false
	
	label.text = dialog[phrase_num]
	label.visible_characters = 0
	while label.visible_characters < len(label.text):
		label.visible_characters += 1
		timer.start()
		yield(timer, "timeout")
	timer.start()
	yield(timer, "timeout")
	indicator.visible = true
	finished = true
	phrase_num += 1
	return

func close():
	menu_player.hide_menu(self)
	yield(menu_player, "animation_finished")
	emit_signal("dialogue_finished")

func _on_SkipButton_pressed():
	close()

extends PanelContainer

onready var fullscreen_button = $MarginContainer/V/FullScreen/CheckButton
onready var title_button = $MarginContainer/V/H/TitleButton
onready var save_button = $MarginContainer/V/H/SaveButton
onready var bgm_slider = $MarginContainer/V/Bgm/HSlider
onready var sfx_slider = $MarginContainer/V/Sfx/HSlider
onready var bgm_label = $MarginContainer/V/Bgm/BgmVolumeLabel
onready var sfx_label = $MarginContainer/V/Sfx/SfxVolumeLabel
onready var ambience_label = $MarginContainer/V/Ambience/AmbienceVolumeLabel
onready var ambience_slider = $MarginContainer/V/Ambience/HSlider
onready var game_sfx_label = $MarginContainer/V/GameSfx/GameSfxVolumeLabel
onready var game_sfx_slider = $MarginContainer/V/GameSfx/HSlider
onready var anim_player = $MenuAnimationPlayer

export(bool) var show_title_button = true
export(bool) var show_save_button = true

func _ready():
	title_button.visible = show_title_button
	save_button.visible = show_save_button
	hide()
	fullscreen_button.pressed = OS.window_fullscreen
	bgm_label.text = str(AudioManager.bgm_volume)
	bgm_slider.value = AudioManager.bgm_volume
	sfx_label.text = str(AudioManager.sfx_volume)
	sfx_slider.value = AudioManager.sfx_volume
	ambience_slider.value = AudioManager.ambience_volume
	ambience_label.text = str(AudioManager.ambience_volume)
	game_sfx_label.text = str(AudioManager.game_sfx_volume)
	game_sfx_slider.value = AudioManager.game_sfx_volume
	EventBus.connect("settings_menu_toggled", self, "_on_settings_menu_toggled")

func _process(_delta):
	if Input.is_action_just_pressed("settings"):
		anim_player.toggle_menu(self)

func _on_settings_menu_toggled():
	anim_player.toggle_menu(self)

func _on_CancelButton_pressed():
	anim_player.hide_menu(self)

func _on_CheckButton_toggled(button_pressed):
	OS.window_fullscreen = button_pressed

func _on_BgmSlider_value_changed(value):
	bgm_label.text = str(value)
	AudioManager.update_bgm_volume(value)

func _on_SfxSlider_value_changed(value):
	sfx_label.text = str(value)
	AudioManager.update_sfx_volume(value)

func _on_AmbienceSlider_value_changed(value):
	ambience_label.text = str(value)
	AudioManager.update_ambience_volume(value)

func _on_QuitButton_pressed():
	get_tree().quit()

func _on_TitleButton_pressed():
	self.hide()
	ActionManager.set_action(Constants.ACTION.SELECT)
	ActionManager.clear()
	EventBus.emit_signal("transition_triggered", Constants.PATH_TITLE_SCREEN)

func _on_SaveButton_pressed():
	EventBus.emit_signal("game_save_triggered")

func _on_GameSfxSlider_value_changed(value):
	game_sfx_label.text = str(value)
	AudioManager.update_game_sfx_volume(value)

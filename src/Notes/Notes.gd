extends Control

onready var note1 = $P/M/V/Labels/Note1
onready var note2 = $P/M/V/Labels/Note2
onready var note3 = $P/M/V/Labels/Note3
onready var note4 = $P/M/V/Labels/Note4
onready var page = $P/M/V/H/Page
var notes: Array = []
var note_index = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	notes = [note1, note2, note3, note4]
	display_note(note_index)

func display_note(new_index):
	page.text = str(note_index + 1) + "/" + str(notes.size())
	for i in range(0, notes.size()):
		notes[i].visible = i == new_index

func toggle_menu():
	$MenuAnimationPlayer.toggle_menu(self)

func _on_NextButton_pressed():
	if note_index == notes.size() - 1:
		note_index = 0
	else:
		note_index += 1
	display_note(note_index)

func _on_BackButton_pressed():
	if note_index == 0:
		note_index = notes.size() - 1
	else:
		note_index -= 1
	display_note(note_index)

func _on_CancelButton_pressed():
	$MenuAnimationPlayer.hide_menu(self)

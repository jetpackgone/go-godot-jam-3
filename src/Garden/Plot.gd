extends Node2D

onready var water_timer = $WaterTimer
onready var sprite = $Sprite
onready var plant_position = $PlantPosition2D

var plant setget ,get_plant
var watered: bool = false

func _ready():
	$Sprite/AnimationPlayer.play("dried")

func _on_MouseDetectionArea_input_event(_viewport, event, _shape_idx):
	if DevMode.enabled && event is InputEventMouseButton && event.button_index == BUTTON_RIGHT && event.pressed:
		var selected_plant = get_plant()
		if selected_plant != null:
			selected_plant.pass_time()
		else:
			var new_plant = load(Constants.PATH_GOGA).instance()
			plant_position.add_child(new_plant)
			if watered:
				new_plant.growth_timer.start()
	if event is InputEventMouseButton && event.button_index == BUTTON_LEFT && event.pressed:
		var selected_plant = get_plant()
		match ActionManager.action:
			Constants.ACTION.PLANT:
				if selected_plant == null:
					$DigSfxPlayer.play()
					var new_plant = load(Constants.PATH_GOGA).instance()
					if !ActionManager.selected_data.empty():
						new_plant.genes = ActionManager.selected_data.genes.duplicate()
						ActionManager.remove_selected_seed()
					plant_position.add_child(new_plant)
					if watered:
						new_plant.growth_timer.start()
					if GameStateManager.tutorial_index == 0:
						EventBus.emit_signal("hint_triggered", Constants.tutorial_hints[GameStateManager.tutorial_index])
						GameStateManager.tutorial_index += 1
					elif GameStateManager.tutorial_index >= Constants.tutorial_hints.size():
						EventBus.emit_signal("hint_hidden")
			Constants.ACTION.WATER:
				if GameStateManager.tutorial_index == 1:
					EventBus.emit_signal("hint_triggered", Constants.tutorial_hints[GameStateManager.tutorial_index])
					GameStateManager.tutorial_index += 1
				water()
			Constants.ACTION.HARVEST:
				if selected_plant != null && selected_plant.has_fruit:
					$BushSfxPlayer.play()
					selected_plant.harvest()
					if GameStateManager.tutorial_index == 3:
						EventBus.emit_signal("hint_triggered", Constants.tutorial_hints[GameStateManager.tutorial_index])
						GameStateManager.tutorial_index += 1
			Constants.ACTION.REMOVE:
				if selected_plant != null:
					$BushSfxPlayer.play()
					GameStateManager.add_seed(selected_plant.genes)
					selected_plant.queue_free()
					EventBus.emit_signal("hint_hidden")
			Constants.ACTION.POLLINATE:
				if selected_plant != null:
					if ActionManager.has_pollen():
						$BeeSfxPlayer.play()
						selected_plant.pollinate(ActionManager.pollen.duplicate())
						ActionManager.pollen = {}
						EventBus.emit_signal("cross_pollinated")
						EventBus.emit_signal("hint_triggered", "Cross-pollinated!")
					else:
						$BushSfxPlayer.play()
						ActionManager.pollen = selected_plant.genes.duplicate()
						EventBus.emit_signal("pollen_gathered")
						EventBus.emit_signal("hint_triggered", "Gathered pollen! Select a plant to pollinate.")
		get_tree().set_input_as_handled()

func _on_WaterTimer_timeout():
	watered = false
	var selected_plant = get_plant()
	if selected_plant != null:
		selected_plant.growth_timer.paused = true

func get_plant():
	for child in plant_position.get_children():
		if child.is_in_group("plant"):
			return child
	return null

func water():
	watered = true
	$WaterSfxPlayer.play()
	$Sprite/AnimationPlayer.play("RESET")
	$Sprite/AnimationPlayer.play("dry")
	water_timer.start()
	$WaterParticles.emitting = true
	var selected_plant = get_plant()
	if selected_plant != null:
		selected_plant.growth_timer.paused = false
		if selected_plant.growth_timer.is_stopped():
			selected_plant.growth_timer.start()

func _on_MouseDetectionArea_mouse_entered():
	sprite.material = load(Constants.OUTLINE_SHADER)
	var selected_plant = get_plant()
	if selected_plant != null:
		if ActionManager.action == Constants.ACTION.POLLINATE:
			if ActionManager.has_pollen():
				EventBus.emit_signal("pollinate_tool_tip_shown", self.position, selected_plant.genes)
		else:
			if ActionManager.action == Constants.ACTION.REMOVE:
				selected_plant.show_scythe(true)
			EventBus.emit_signal("plant_tool_tip_shown", self.position, selected_plant.genes)

func _on_MouseDetectionArea_mouse_exited():
	sprite.material = null
	var selected_plant = get_plant()
	if selected_plant != null:
		selected_plant.show_scythe(false)
	EventBus.emit_signal("plant_tool_tip_hidden")
	EventBus.emit_signal("pollinate_tool_tip_hidden")

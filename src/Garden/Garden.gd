extends Node2D

onready var grid = $Grid

# Called when the node enters the scene tree for the first time.
func _ready():
	EventBus.connect("game_save_triggered", self, "_on_game_save_triggered")
	load_from_save()

func _process(_delta):
	if ActionManager.action == Constants.ACTION.WATER:
		if Input.is_mouse_button_pressed(BUTTON_LEFT):
			var water_particles = load(Constants.WATER_PARTICLES).instance()
			water_particles.global_position == get_global_mouse_position()
			add_child(water_particles)
			water_particles.emit()

func _on_game_save_triggered():
	# Allocating new array since resoure confuses references when updating existing array
	# Possibly related to https://github.com/godotengine/godot/issues/55885
	var new_plants = []
	for plant in get_tree().get_nodes_in_group("plant"):
		var plant_data = plant.get_data()
		new_plants.append(plant_data)
	GameStateManager.plants = new_plants
	
	SaveSystem.save_game()

func load_from_save():
	for plant_data in GameStateManager.plants:
		var goga = load(Constants.PATH_GOGA).instance()
		var plot = grid.get_child(plant_data.garden_index)
		plot.plant_position.add_child(goga)
		goga.load_data(plant_data)
		if plot.watered:
			goga.growth_timer.start()

func pass_time():
	for plant in get_tree().get_nodes_in_group("plant"):
		plant.pass_time()

extends Control

onready var status_label = $StatusLabel
onready var inventory = $MarginContainer/Inventory
onready var hint = $M5/HintLabel
onready var seed_tool_tip = $SeedToolTip
onready var fruit_tool_tip = $FruitToolTip
onready var plant_tool_tip = $PlantToolTip
onready var pollinate_tool_tip = $PollinateToolTip

func _ready():
	EventBus.connect("game_saved", self, "_on_game_saved")
	EventBus.connect("cross_pollinated", self, "_on_cross_pollinated")
	EventBus.connect("pollen_gathered", self, "_on_pollen_gathered")
	EventBus.connect("action_selected", self, "_on_action_selected")
	EventBus.connect("dev_mode_toggled", self, "_on_dev_mode_toggled")
	EventBus.connect("seed_tool_tip_shown", self, "_on_seed_tool_tip_shown")
	EventBus.connect("seed_tool_tip_hidden", self, "_on_seed_tool_tip_hidden")
	EventBus.connect("fruit_tool_tip_shown", self, "_on_fruit_tool_tip_shown")
	EventBus.connect("fruit_tool_tip_hidden", self, "_on_fruit_tool_tip_hidden")
	EventBus.connect("plant_tool_tip_shown", self, "_on_plant_tool_tip_shown")
	EventBus.connect("plant_tool_tip_hidden", self, "_on_plant_tool_tip_hidden")
	EventBus.connect("pollinate_tool_tip_shown", self, "_on_pollinate_tool_tip_shown")
	EventBus.connect("pollinate_tool_tip_hidden", self, "_on_pollinate_tool_tip_hidden")
	ActionManager.set_action(Constants.ACTION.SELECT)
	seed_tool_tip.hide()
	fruit_tool_tip.hide()
	plant_tool_tip.hide()
	pollinate_tool_tip.hide()
	inventory.hide()

func _process(delta):
	if GameStateManager.mode != Constants.GAME_MODE.GARDEN:
		return
	
	if Input.is_action_pressed("select_mode"):
		ActionManager.set_action(Constants.ACTION.SELECT)
	elif Input.is_action_pressed("water_mode"):
		ActionManager.set_action(Constants.ACTION.WATER)
	elif Input.is_action_pressed("harvest_mode"):
		ActionManager.set_action(Constants.ACTION.HARVEST)
	elif Input.is_action_pressed("remove_mode"):
		ActionManager.set_action(Constants.ACTION.REMOVE)
	elif Input.is_action_pressed("pollinate_mode"):
		ActionManager.set_action(Constants.ACTION.POLLINATE)
	elif Input.is_action_just_released("mouse_scroll_down"):
		var action_index = ActionManager.action + 1
		if ActionManager.action == Constants.ACTION.POLLINATE:
			action_index = Constants.ACTION.SELECT
		ActionManager.set_action(action_index)
		$MenuSelectSfxPlayer.play()
	elif Input.is_action_just_released("mouse_scroll_up"):
		var action_index = ActionManager.action - 1
		if ActionManager.action == Constants.ACTION.SELECT:
			action_index = Constants.ACTION.POLLINATE
		ActionManager.set_action(action_index)
		$MenuSelectSfxPlayer.play()

func _on_seed_tool_tip_shown(rect: Rect2):
	seed_tool_tip.refresh()
	seed_tool_tip.rect_position.y = rect.position.y
	seed_tool_tip.show()

func _on_seed_tool_tip_hidden():
	seed_tool_tip.hide()

func _on_fruit_tool_tip_shown(rect: Rect2):
	fruit_tool_tip.refresh()
	fruit_tool_tip.rect_position.y = rect.position.y
	fruit_tool_tip.show()

func _on_fruit_tool_tip_hidden():
	fruit_tool_tip.hide()

func _on_plant_tool_tip_shown(pos: Vector2, genes: Dictionary):
	plant_tool_tip.set_genes(genes)
	plant_tool_tip.rect_position = pos - Vector2(400, 225)
	plant_tool_tip.show()

func _on_plant_tool_tip_hidden():
	plant_tool_tip.hide()

func _on_pollinate_tool_tip_shown(pos: Vector2, genes: Dictionary):
	pollinate_tool_tip.set_genes(ActionManager.pollen, genes)
	pollinate_tool_tip.rect_position = pos - Vector2(400, 225)
	pollinate_tool_tip.show()

func _on_pollinate_tool_tip_hidden():
	pollinate_tool_tip.hide()

func _on_dev_mode_toggled(enabled):
	$StatusLabel.visible = enabled

func _on_cross_pollinated():
	ActionManager.set_action(Constants.ACTION.POLLINATE)
	status_label.text = "Cross pollinated"

func _on_pollen_gathered():
	ActionManager.set_action(Constants.ACTION.POLLINATE)
	status_label.text = "Gathered pollen"

func _on_action_selected(new_action: int):
	select_action(new_action)

func _on_game_saved():
	$M2/GameSavedLabel.show()
	$M2/Timer.start()
	yield($M2/Timer, "timeout")
	$M2/GameSavedLabel.hide()

func _on_PollinateButton_pressed():
	ActionManager.set_action(Constants.ACTION.POLLINATE)

func _on_SelectButton_pressed():
	ActionManager.set_action(Constants.ACTION.SELECT)

func _on_WateringCanButton_pressed():
	ActionManager.set_action(Constants.ACTION.WATER)

func _on_HarvestButton_pressed():
	ActionManager.set_action(Constants.ACTION.HARVEST)

func _on_RemoveButton_pressed():
	ActionManager.set_action(Constants.ACTION.REMOVE)

func select_action(new_action):
	$V/H/SelectButton.pressed = new_action == Constants.ACTION.SELECT
	$V/H/WateringCanButton.pressed = new_action == Constants.ACTION.WATER
	$V/H/HarvestButton.pressed = new_action == Constants.ACTION.HARVEST
	$V/H/RemoveButton.pressed = new_action == Constants.ACTION.REMOVE
	$V/H/PollinateButton.pressed = new_action == Constants.ACTION.POLLINATE
	$V/ActionLabel.text = Constants.ACTION.keys()[new_action]

func _on_CafeButton_pressed():
	if GameStateManager.tutorial_index >= Constants.tutorial_hints.size():
		EventBus.emit_signal("hint_hidden")
	EventBus.emit_signal("game_mode_swapped", Constants.GAME_MODE.CAFE)

func _on_NotesButton_pressed():
	$Notes.toggle_menu()

func _on_SettingsButton_pressed():
	ActionManager.set_action(Constants.ACTION.SELECT)
	EventBus.emit_signal("settings_menu_toggled")

func _on_InventoryButton_pressed():
	inventory.toggle_menu()

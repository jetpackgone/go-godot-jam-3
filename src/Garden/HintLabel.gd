extends Label

func _ready():
	EventBus.connect("hint_triggered", self, "_on_hint_triggered")
	EventBus.connect("hint_hidden", self, "_on_hint_hidden")

func _on_hint_triggered(hint_text):
	text = hint_text
	show()

func _on_hint_hidden():
	hide()

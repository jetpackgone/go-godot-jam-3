extends MarginContainer

onready var seeds = $V/Tabs/Seeds
onready var fruits = $V/Tabs/Fruits

func _ready():
	EventBus.connect("delete_mode_toggled", self, "_on_delete_mode_toggled")
	$V/Tabs.set_tab_icon(0, load("res://Garden/UI/seed_icon.png"))
	$V/Tabs.set_tab_icon(1, load("res://Garden/UI/fruit_icon.png"))
	$V/Tabs.set_tab_title(0, "")
	$V/Tabs.set_tab_title(1, "")

func toggle_menu():
	$V/DiscardButton.pressed = false
	set_delete_mode(false)
	$MenuAnimationPlayer.toggle_menu(self)

func _on_Seeds_seed_selected(_seed_entry):
	toggle_menu()
	EventBus.emit_signal("hint_triggered", "Select a plot to plant the seed!")

func set_delete_mode(is_delete_mode):
	GameStateManager.set_delete_mode(is_delete_mode)

func _on_Button_toggled(button_pressed):
	set_delete_mode(button_pressed)

func _on_delete_mode_toggled(toggled):
	$V/DiscardButton.pressed = toggled

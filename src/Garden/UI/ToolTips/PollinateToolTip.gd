extends PanelContainer

onready var color_cross = $M/V/M/V/Color/Cross
onready var shape_cross = $M/V/M/V/Shape/Cross
onready var taste_cross = $M/V/M/V/Taste/Cross

func refresh():
	if ActionManager.previewed_seed_data.empty():
		return
	set_genes(ActionManager.previewed_seed_data.parent_genes_1, ActionManager.previewed_seed_data.parent_genes_2)

func set_genes(parent1: Dictionary, parent2: Dictionary):
	if parent1.empty() || parent2.empty():
		return
	var genotypes1 = GeneHelper.get_genotypes(parent1)
	var genotypes2 = GeneHelper.get_genotypes(parent2)
	color_cross.text = GeneHelper.get_color_genotype(parent1[Constants.GENE_COLOR]) + " x " + GeneHelper.get_color_genotype(parent2[Constants.GENE_COLOR])
	shape_cross.text = GeneHelper.get_shape_genotype(parent1[Constants.GENE_SHAPE]) + " x " + GeneHelper.get_shape_genotype(parent2[Constants.GENE_SHAPE])
	taste_cross.text = GeneHelper.get_taste_genotype(parent1[Constants.GENE_TASTE]) + " x " + GeneHelper.get_taste_genotype(parent2[Constants.GENE_TASTE])

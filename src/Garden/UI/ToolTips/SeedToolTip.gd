extends PanelContainer

onready var color_geno = $M/V/M/V/Color/Genotype
onready var shape_geno = $M/V/M/V/Shape/Genotype
onready var taste_geno = $M/V/M/V/Taste/Genotype

func refresh():
	if ActionManager.previewed_seed_data.empty():
		return
	set_genes(ActionManager.previewed_seed_data.genes)

func set_genes(genes: Dictionary):
	if genes.empty():
		return
	var genotypes = GeneHelper.get_genotypes(genes)
	color_geno.text = genotypes[Constants.GENE_COLOR]
	shape_geno.text = genotypes[Constants.GENE_SHAPE]
	taste_geno.text = genotypes[Constants.GENE_TASTE]

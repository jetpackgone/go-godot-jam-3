extends PanelContainer

onready var color_geno = $M/V/M/V/Color/Genotype
onready var shape_geno = $M/V/M/V/Shape/Genotype
onready var taste_geno = $M/V/M/V/Taste/Genotype
onready var color_pheno = $M/V/M/V/Color/Phenotype
onready var shape_pheno = $M/V/M/V/Shape/Phenotype
onready var taste_pheno = $M/V/M/V/Taste/Phenotype

func refresh():
	if ActionManager.previewed_seed_data.empty():
		return
	set_genes(ActionManager.previewed_seed_data.genes)

func set_genes(genes: Dictionary):
	if genes.empty():
		return
	var genotypes = GeneHelper.get_genotypes(genes)
	color_geno.text = genotypes[Constants.GENE_COLOR]
	shape_geno.text = genotypes[Constants.GENE_SHAPE]
	taste_geno.text = genotypes[Constants.GENE_TASTE]
	var phenotypes = GeneHelper.get_phenotypes(genes)
	color_pheno.text = phenotypes[Constants.GENE_COLOR]
	shape_pheno.text = phenotypes[Constants.GENE_SHAPE]
	taste_pheno.text = phenotypes[Constants.GENE_TASTE]

extends Button

onready var texture_rect = $TextureRect

signal selected(entry)

var data: Dictionary = {}
var delete_mode := false

var SPIKY_RED = "res://Garden/UI/spiky_red.tres"
var SPIKY_YELLOW = "res://Garden/UI/spiky_yellow.tres"
var SMOOTH_RED = "res://Garden/UI/smooth_red.tres"
var SMOOTH_YELLOW = "res://Garden/UI/smooth_yellow.tres"

func _ready():
	if self.data.empty():
		return
	set_phenotype()
	EventBus.connect("delete_mode_toggled", self, "_on_delete_mode_toggled")
	toggle_delete_mode(GameStateManager.delete_mode)

func _on_delete_mode_toggled(is_delete_mode: bool):
	toggle_delete_mode(is_delete_mode)

func _on_FruitEntry_toggled(button_pressed):
	if button_pressed:
		emit_signal("selected", self)

func set_phenotype():
	var phenotypes = GeneHelper.get_phenotypes(self.data.genes)

	if phenotypes[Constants.GENE_COLOR] == "red":
		if phenotypes[Constants.GENE_SHAPE] == "spiky":
			texture_rect.texture = load(SPIKY_RED)
		else:
			texture_rect.texture = load(SMOOTH_RED)
	else:
		if phenotypes[Constants.GENE_SHAPE] == "spiky":
			texture_rect.texture = load(SPIKY_YELLOW)
		else:
			texture_rect.texture = load(SMOOTH_YELLOW)

func _on_FruitEntry_pressed():
	if delete_mode && get_parent().get_child_count() > 1:
		$BushSfxPlayer.play()
		GameStateManager.remove_fruit(data)
		EventBus.emit_signal("fruit_tool_tip_hidden")
	else:
		$ConfirmSfxPlayer.play()

func _on_FruitEntry_mouse_entered():
	ActionManager.previewed_seed_data = data
	$MenuSelectSfxPlayer.play()
	$AnimationPlayer.play("vibrate")
	if delete_mode:
		$TextureRect/TrashCan.play()
	EventBus.emit_signal("fruit_tool_tip_shown", self.get_global_rect())

func _on_FruitEntry_mouse_exited():
	$AnimationPlayer.play("RESET")
	if delete_mode:
		$TextureRect/TrashCan.stop()
	EventBus.emit_signal("fruit_tool_tip_hidden")

func toggle_delete_mode(is_delete_mode: bool):
	delete_mode = is_delete_mode
	if delete_mode:
		$TextureRect/TrashCan.show()
	else:
		$TextureRect/TrashCan.hide()

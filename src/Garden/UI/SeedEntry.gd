extends Button

signal selected(button)

var data: Dictionary = {}
var delete_mode := false

func _ready():
	EventBus.connect("delete_mode_toggled", self, "_on_delete_mode_toggled")
	toggle_delete_mode(GameStateManager.delete_mode)

func _on_delete_mode_toggled(is_delete_mode: bool):
	toggle_delete_mode(is_delete_mode)

func _on_SeedEntry_toggled(button_pressed):
	if button_pressed && !delete_mode:
		EventBus.emit_signal("seed_tool_tip_hidden")
		ActionManager.set_seed_to_plant(self)
		emit_signal("selected", self)

func _on_SeedEntry_pressed():
	if delete_mode && get_parent().get_child_count() > 1:
		$BushSfxPlayer.play()
		GameStateManager.remove_seed(data)
		EventBus.emit_signal("seed_tool_tip_hidden")
	else:
		$ConfirmSfxPlayer.play()

func _on_SeedEntry_mouse_entered():
	ActionManager.previewed_seed_data = data
	$MenuSelectSfxPlayer.play()
	$AnimationPlayer.play("vibrate")
	if delete_mode:
		$TextureRect/TrashCan.play()
	EventBus.emit_signal("seed_tool_tip_shown", self.get_global_rect())

func _on_SeedEntry_mouse_exited():
	$AnimationPlayer.play("RESET")
	if delete_mode:
		$TextureRect/TrashCan.stop()
	EventBus.emit_signal("seed_tool_tip_hidden")

func toggle_delete_mode(is_delete_mode: bool):
	delete_mode = is_delete_mode
	if delete_mode:
		$TextureRect/TrashCan.show()
	else:
		$TextureRect/TrashCan.hide()

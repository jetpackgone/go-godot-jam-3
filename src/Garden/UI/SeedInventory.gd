extends PanelContainer

signal seed_selected(seed_entry)

onready var grid = $S/M/Grid

func _ready():
	EventBus.connect("seed_added", self, "_on_seed_added")
	EventBus.connect("seed_removed", self, "_on_seed_removed")
	refresh()

func _on_seed_added(seed_data: Dictionary):
	var new_entry = load(Constants.PATH_SEED_ENTRY).instance()
	new_entry.data = seed_data
	grid.add_child(new_entry)
	new_entry.connect("selected", self, "_on_seed_entry_selected")

func _on_seed_removed(seed_data: Dictionary):
	for child in grid.get_children():
		if child.data == seed_data:
			child.queue_free()

func refresh():
	for child in grid.get_children():
		child.queue_free()
	for seed_data in GameStateManager.seed_inventory:
		var new_seed_entry = load(Constants.PATH_SEED_ENTRY).instance()
		new_seed_entry.data = seed_data
		grid.add_child(new_seed_entry)
		new_seed_entry.connect("selected", self, "_on_seed_entry_selected")

func _on_seed_entry_selected(seed_entry):
	for child in grid.get_children():
		child.pressed = child == seed_entry
	emit_signal("seed_selected", seed_entry)

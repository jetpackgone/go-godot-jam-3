extends PanelContainer

signal fruit_selected(entry)

onready var grid = $S/M/Grid

# Called when the node enters the scene tree for the first time.
func _ready():
	EventBus.connect("fruit_added", self, "_on_fruit_added")
	EventBus.connect("fruit_removed", self, "_on_fruit_removed")
	refresh()

func _on_fruit_added(fruit_data: Dictionary):
	var new_entry = load(Constants.PATH_FRUIT_ENTRY).instance()
	new_entry.data = fruit_data
	grid.add_child(new_entry)
	new_entry.connect("selected", self, "_on_entry_selected")

func _on_fruit_removed(fruit_data: Dictionary):
	for child in grid.get_children():
		if child.data == fruit_data:
			child.queue_free()

func refresh():
	for child in grid.get_children():
		child.queue_free()
	for data in GameStateManager.fruit_inventory:
		var new_entry = load(Constants.PATH_FRUIT_ENTRY).instance()
		new_entry.data = data
		grid.add_child(new_entry)
		new_entry.connect("selected", self, "_on_entry_selected")
		
func _on_entry_selected(entry):
	for child in grid.get_children():
		child.pressed = child == entry
	emit_signal("fruit_selected", entry)

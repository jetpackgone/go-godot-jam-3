extends Button

func _on_Button_pressed():
	$ConfirmSfxPlayer.play()

func _on_Button_mouse_entered():
	if !pressed:
		$MenuSelectSfxPlayer.play()

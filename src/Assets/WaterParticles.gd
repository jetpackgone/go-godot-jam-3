extends CPUParticles2D

func emit():
	emitting = true
	$Timer.start()

func _on_Timer_timeout():
	queue_free()

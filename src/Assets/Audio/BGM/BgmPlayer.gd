extends AudioStreamPlayer

var fading_out := false

func _ready():
	EventBus.connect("bgm_volume_updated", self, "_on_bgm_volume_updated")
	volume_db = linear2db(AudioManager.bgm_volume * 0.01)

func _on_bgm_volume_updated(volume: float):
	volume_db = linear2db(volume * 0.01)

func fade_out(duration: float = 1.0):
	$Tween.stop_all()
	fading_out = true
	$Tween.interpolate_property(self, "volume_db", volume_db, -80, duration)
	$Tween.start()

func fade_in(duration: float = 1.0):
	$Tween.stop_all()
	fading_out = false
	volume_db = -80
	var target = linear2db(AudioManager.ambience_volume * 0.01)
	play()
	$Tween.interpolate_property(self, "volume_db", volume_db, target, duration)
	$Tween.start()

func _on_Tween_tween_all_completed():
	if fading_out:
		stop()

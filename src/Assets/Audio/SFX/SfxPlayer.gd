extends AudioStreamPlayer

var rng: RandomNumberGenerator

func _ready():
	rng = RandomNumberGenerator.new()
	EventBus.connect("sfx_volume_updated", self, "_on_sfx_volume_updated")
	volume_db = linear2db(AudioManager.sfx_volume * 0.01)

func _on_sfx_volume_updated(volume: float):
	volume_db = linear2db(volume * 0.01)

func randomize_pitch():
	rng.randomize()
	pitch_scale = 1.2 + rng.randf_range(-0.05, 0.05)

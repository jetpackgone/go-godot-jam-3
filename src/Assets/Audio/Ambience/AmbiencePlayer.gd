extends AudioStreamPlayer

var fading_out := false

func _ready():
	EventBus.connect("ambience_volume_updated", self, "_on_volume_updated")
	volume_db = linear2db(AudioManager.ambience_volume * 0.01)

func _on_volume_updated(volume: float):
	volume_db = linear2db(volume * 0.01)

func fade_out():
	$Tween.stop_all()
	fading_out = true
	$Tween.interpolate_property(self, "volume_db", volume_db, -80, 1.0)
	$Tween.start()

func fade_in():
	$Tween.stop_all()
	fading_out = false
	volume_db = -80
	var target = linear2db(AudioManager.ambience_volume * 0.01)
	play()
	$Tween.interpolate_property(self, "volume_db", volume_db, target, 1.0)
	$Tween.start()

func _on_Tween_tween_all_completed():
	if fading_out:
		stop()

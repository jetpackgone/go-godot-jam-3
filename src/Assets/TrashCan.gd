extends TextureRect

func _ready():
	hide()

func play():
	$AnimationPlayer.play("hover")

func stop():
	$AnimationPlayer.play("RESET")

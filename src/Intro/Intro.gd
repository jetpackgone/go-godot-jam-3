extends Node2D

onready var dialogue_box = $CanvasLayer/DialogueBox

var dialog = [
	"Welcome to Goga Boba!",
	"You are playing as the founder and manager of the popular Goga Boba cafe.",
	"The cafe relies on a garden that produces a variety of fruit that are used to create the signature Goga Boba tea.",
	"The fruit was discovered when you had found a mysterious seed and a number of notes that your grandmother had passed down to you.",
	"The fruit may be sweet or bitter, depending on what type of fruit is grown in the garden, so try to create a ‘fruitful’ field!",
	"The customers seem to enjoy our boba drinks (most of the time)."
]

func _ready():
	EventBus.connect("transition_finished", self, "_on_transition_finished")

func _on_transition_finished():
	dialogue_box.dialog = dialog
	dialogue_box.start()

func _on_DialogueBox_dialogue_finished():
	EventBus.emit_signal("transition_triggered", Constants.PATH_GAME)

func _on_SkipButton_pressed():
	$CanvasLayer/DialogueBox.hide()
	$CanvasLayer/MarginContainer/SkipButton.hide()
	EventBus.emit_signal("transition_triggered", Constants.PATH_GAME)

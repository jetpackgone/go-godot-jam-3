extends Node

var GENE_COLOR = "color"
var GENE_SHAPE = "shape"
var GENE_TASTE = "taste"

var PATH_INTRO = "res://Intro/Intro.tscn"
var PATH_TITLE_SCREEN = "res://TitleScreen/TitleScreen.tscn"
var PATH_GAME = "res://Game.tscn"
var PATH_GOGA = "res://Goga/Goga.tscn"
var PATH_GARDEN = "res://Garden/Garden.tscn"
var PATH_FRUIT = "res://Goga/Fruit/Fruit.tscn"

var PLANT_ADULT_TEXTURE = "res://Goga/adult_texture.tres"
var PLANT_FLOWER_TEXTURE = "res://Goga/flower_texture.tres"
var PLANT_SPROUT_TEXTURE = "res://Goga/sprout_texture.tres"
var PLANT_SEED_TEXTURE = "res://Goga/seed_texture.tres"

var PATH_SEED_ENTRY = "res://Garden/UI/SeedEntry.tscn"
var PATH_FRUIT_ENTRY = "res://Garden/UI/FruitEntry.tscn"

var OUTLINE_SHADER = "res://Assets/Shaders/outline_shader.tres"

var WATER_PARTICLES = "res://Assets/WaterParticles.tscn"

var CURSOR_HAND_POINT = "res://Assets/cursor_hand_point.png"
var CURSOR_HAND_GRAB = "res://Assets/cursor_hand_grab.png"
var CURSOR_WATERING_CAN = "res://Assets/cursor_watering_can.png"
var CURSOR_TROWEL = "res://Assets/cursor_trowel.png"
var CURSOR_SCYTHE = "res://Assets/cursor_scythe.png"
var CURSOR_POLLINATE = "res://Assets/cursor_pollinate.png"
var CURSOR_POLLINATE_BEE = "res://Assets/cursor_pollinate_bee.png"

var PORTRAIT_GIRL_DISGUSTED = "res://Intro/girl_disgusted.tres"
var PORTRAIT_GIRL_HAPPY = "res://Intro/girl_happy.tres"
var PORTRAIT_GIRL_NEUTRAL = "res://Intro/girl_neutral.tres"
var PORTRAIT_GIRL_UNHAPPY = "res://Intro/girl_unhappy.tres"

var PORTRAIT_MAN_DISGUSTED = "res://Intro/man_disgusted.tres"
var PORTRAIT_MAN_HAPPY = "res://Intro/man_happy.tres"
var PORTRAIT_MAN_NEUTRAL = "res://Intro/man_neutral.tres"
var PORTRAIT_MAN_UNHAPPY = "res://Intro/man_unhappy.tres"

var PORTRAIT_OLD_WOMAN_DISGUSTED = "res://Intro/old_woman_disgusted.tres"
var PORTRAIT_OLD_WOMAN_HAPPY = "res://Intro/old_woman_happy.tres"
var PORTRAIT_OLD_WOMAN_NEUTRAL = "res://Intro/old_woman_neutral.tres"
var PORTRAIT_OLD_WOMAN_UNHAPPY = "res://Intro/old_woman_unhappy.tres"

var FLOATING_NUMBER = "res://Cafe/FloatingNumber.tscn"

var portraits = {
	"man": {
		"neutral": PORTRAIT_MAN_NEUTRAL,
		"happy": PORTRAIT_MAN_HAPPY,
		"unhappy": PORTRAIT_MAN_UNHAPPY,
		"disgusted": PORTRAIT_MAN_DISGUSTED
	},
	"girl": {
		"neutral": PORTRAIT_GIRL_NEUTRAL,
		"happy": PORTRAIT_GIRL_HAPPY,
		"unhappy": PORTRAIT_GIRL_UNHAPPY,
		"disgusted": PORTRAIT_GIRL_DISGUSTED
	},
	"old_woman": {
		"neutral": PORTRAIT_OLD_WOMAN_NEUTRAL,
		"happy": PORTRAIT_OLD_WOMAN_HAPPY,
		"unhappy": PORTRAIT_OLD_WOMAN_UNHAPPY,
		"disgusted": PORTRAIT_OLD_WOMAN_DISGUSTED
	},
}

var bitter_reactions = [
	"Blargh! This is bitter!",
	"Ew, this tastes terrible!",
	"This is disgusting..."
]

var wrong_color_reactions = [
	"This isn't the color I wanted.",
	"But I wanted the other color..."
]

var wrong_shape_reactions = [
	"The texture seems off...",
	"Not the shape I wanted."
]

var perfect_reactions = [
	"Perfect, thank you!",
	"So delicious and sweet!",
	"Exactly like I wanted, yay!"
]

var REPUTATION_PERFECT = 10
var REPUTATION_OK = 5
var REPUTATION_FAIL = -10

enum ACTION {SELECT, WATER, HARVEST, REMOVE, POLLINATE, PLANT}
enum GROWTH_STAGE {SEED, SPROUT, ADULT, FLOWER, FRUIT}
enum GAME_MODE {GARDEN, CAFE}

var tutorial_hints = [
	"Water your plant with the watering can!",
	"Water more as needed until ready for harvest.",
	"Your Goga plant is ready for harvest!",
	"Harvesting a fruit also gives you a seed. Go to the cafe when you have enough fruits!"
]

extends Node

var enabled: bool = false

func _process(_delta):
	if Input.is_action_just_pressed("dev_mode"):
		enabled = !enabled
		EventBus.emit_signal("dev_mode_toggled", enabled)

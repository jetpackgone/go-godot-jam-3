extends Node

var game_save_mutex: Mutex
var save_directory: String = "res://saves/"
var dir: Directory
var preloader: ResourcePreloader

func _ready():
	game_save_mutex = Mutex.new()
	dir = Directory.new()
	preloader = ResourcePreloader.new()

func _get_save_path():
	return save_directory + "goga_save.tres"

func load_game() -> void:
	game_save_mutex.lock()
	var file_path = _get_save_path()
	if ResourceLoader.exists(file_path):
		var data = ResourceLoader.load(file_path)
		if data is GameSave:
			GameStateManager.load_game_state(data)
			print("Game loaded successfully")
			EventBus.emit_signal("game_loaded")
			return
	print("Failed to load game. Loading new game...")
	var data = initialize()
	GameStateManager.load_game_state(data)
	game_save_mutex.unlock()
	EventBus.emit_signal("game_loaded")

func load_new_game():
	game_save_mutex.lock()
	var data = initialize()
	GameStateManager.load_game_state(data)
	game_save_mutex.unlock()
	EventBus.emit_signal("game_loaded")

# TODO - Run in background thread?
# TODO - Save to user:// directory instead of res://?
func save_game():
	game_save_mutex.lock()
	GameStateManager.last_save_date = OS.get_unix_time()
	if !dir.dir_exists(save_directory):
		dir.make_dir_recursive(save_directory)
	var file_path = _get_save_path()
	var data = GameStateManager.create_game_save()
	var result = ResourceSaver.save(file_path, data)
	assert(result == OK)
	game_save_mutex.unlock()
	EventBus.emit_signal("game_saved")

func erase_save():
	var file_path = _get_save_path()
	if ResourceLoader.exists(file_path) && dir.dir_exists(save_directory):
		dir.open(save_directory)
		dir.remove(file_path)
	EventBus.emit_signal("game_erased")

func read_save_file(file_path: String):
	if ResourceLoader.exists(file_path):
		var loaded_game_save = ResourceLoader.load(file_path)
		if loaded_game_save is GameSave:
			return loaded_game_save
	return null

func save_exist() -> bool:
	if dir.open(save_directory) == OK:
		dir.list_dir_begin()
		var file_name = dir.get_next()
		while file_name != "":
			if is_valid_save_file(file_name):
				return true
			file_name = dir.get_next()
	return false

func is_valid_save_file(file_name: String) -> bool:
	return file_name.ends_with(".res") || file_name.ends_with(".tres")

func initialize() -> GameSave:
	var game_save = GameSave.new()
	game_save.created_date = OS.get_unix_time()
	game_save.last_save_date = game_save.created_date
	
	var first_seed = {
		"genes": {
			"color": 1,
			"shape": 1,
			"taste": 1
		},
		"parent_genes_1": {},
		"parent_genes_2": {}
	}
	game_save.seeds = [first_seed]
	return game_save

extends Node

signal game_saved
signal game_loaded
signal game_erased
signal game_save_triggered
signal transition_triggered(scene_path)
signal transition_finished
signal dev_mode_toggled(dev_mode)

signal bgm_volume_updated
signal sfx_volume_updated
signal ambience_volume_updated
signal game_sfx_volume_updated

signal harvested(genes, parent_genes_1, parent_genes_2)
signal seed_added(seed_data)
signal seed_removed(seed_data)
signal fruit_added(fruit_data)
signal fruit_removed(fruit_data)
signal pollen_gathered
signal cross_pollinated

signal action_selected(action)
signal game_mode_swapped(new_mode)
signal settings_menu_toggled
signal hint_triggered(hint_text)
signal hint_hidden

signal gave_boba
signal reputation_updated(rep)

signal seed_tool_tip_shown(rect)
signal seed_tool_tip_hidden
signal fruit_tool_tip_shown(rect)
signal fruit_tool_tip_hidden
signal plant_tool_tip_shown(plot_position, genes)
signal plant_tool_tip_hidden
signal pollinate_tool_tip_shown(plot_position, genes)
signal pollinate_tool_tip_hidden

signal delete_mode_toggled(delete_mode)

extends Node

var last_save_date: int = 0
var created_date: int = 0
var plants := []
var seed_inventory := []
var fruit_inventory := []

var intro_done := false
var tutorial_index := 0

var reputation: = 0
var mode = Constants.GAME_MODE.GARDEN
var delete_mode := false

func _ready():
	EventBus.connect("harvested", self, "_on_harvested")

func _on_harvested(genes, parent_genes_1, parent_genes_2):
	var new_genes := {
		"genes": genes,
		"parent_genes_1": parent_genes_1,
		"parent_genes_2": parent_genes_2
	}
	seed_inventory.append(new_genes)
	EventBus.emit_signal("seed_added", new_genes)
	fruit_inventory.append(new_genes)
	EventBus.emit_signal("fruit_added", new_genes)

func remove_seed(seed_data):
	seed_inventory.erase(seed_data)
	EventBus.emit_signal("seed_removed", seed_data)

func remove_fruit(fruit_data):
	fruit_inventory.erase(fruit_data)
	EventBus.emit_signal("fruit_removed", fruit_data)

func load_game_state(data: GameSave):
	plants = data.plants
	created_date = data.created_date
	last_save_date = data.last_save_date
	seed_inventory = data.seeds
	fruit_inventory = data.fruits
	intro_done = data.intro_done
	reputation = data.reputation
	tutorial_index = data.tutorial_index

func create_game_save() -> GameSave:
	var data = GameSave.new()
	data.plants = plants
	data.created_date = created_date
	data.last_save_date = last_save_date
	data.seeds = seed_inventory
	data.fruits = fruit_inventory
	data.intro_done = intro_done
	data.reputation = reputation
	data.tutorial_index = tutorial_index
	return data

func add_seed(seed_genes: Dictionary):
	var new_seed = {
		"genes": seed_genes,
		"parent_genes_1": {},
		"parent_genes_2": {}
	}
	seed_inventory.append(new_seed)
	EventBus.emit_signal("seed_added", new_seed)

func set_delete_mode(is_delete_mode: bool):
	delete_mode = is_delete_mode
	EventBus.emit_signal("delete_mode_toggled", delete_mode)

func add_reputation(added: int):
	reputation = clamp(reputation + added, 0, 100)
	EventBus.emit_signal("reputation_updated", reputation)

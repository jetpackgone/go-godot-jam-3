extends Node

var rng := RandomNumberGenerator.new()

# Calculate possible offspring for one gene with punnett square
func punnett_square(gene_1: int, gene_2: int) -> Array:
	var result = []
	var allele_1a = gene_1 / 2
	var allele_1b = gene_1 % 2
	var allele_2a = gene_2 / 2
	var allele_2b = gene_2 % 2
	result.append(allele_1a * 2 + allele_2a) 
	result.append(allele_1b * 2 + allele_2b)
	result.append(allele_2a * 2 + allele_1b) 
	result.append(allele_2b * 2 + allele_1a) 
	return result

# Calculate punnett square for each gene and randomly generate an offspring
# Can use to self or cross pollinate
func breed(genes_1: Dictionary, genes_2: Dictionary) -> Dictionary:
	var result = {}
	for key in genes_1:
		var possible = punnett_square(genes_1[key], genes_2[key])
		rng.randomize()
		var rand_index = rng.randi_range(0, possible.size() - 1)
		result[key] = possible[rand_index]
	return result

func get_phenotypes(genes: Dictionary) -> Dictionary:
	var result := {}
	
	match genes[Constants.GENE_COLOR]:
		0: result[Constants.GENE_COLOR] = "yellow"
		_: result[Constants.GENE_COLOR] = "red"
	
	match genes[Constants.GENE_SHAPE]:
		0: result[Constants.GENE_SHAPE] = "spiky"
		_: result[Constants.GENE_SHAPE] = "smooth"
	
	match genes[Constants.GENE_TASTE]:
		0: result[Constants.GENE_TASTE] = "sweet"
		_: result[Constants.GENE_TASTE] = "bitter"
	
	# Add more genotype -> phenotype translations here
	return result

func get_genotypes(genes: Dictionary) -> Dictionary:
	var result := {}
	
	match genes[Constants.GENE_COLOR]:
		0: result[Constants.GENE_COLOR] = "rr"
		3: result[Constants.GENE_COLOR] = "RR"
		_: result[Constants.GENE_COLOR] = "Rr"
	
	match genes[Constants.GENE_SHAPE]:
		0: result[Constants.GENE_SHAPE] = "hh"
		3: result[Constants.GENE_SHAPE] = "HH"
		_: result[Constants.GENE_SHAPE] = "Hh"
	
	match genes[Constants.GENE_TASTE]:
		0: result[Constants.GENE_TASTE] = "tt"
		3: result[Constants.GENE_TASTE] = "TT"
		_: result[Constants.GENE_TASTE] = "Tt"
	
	return result

func get_color_genotype(gene: int) -> String:
	match gene:
		0: return "rr"
		3: return "RR"
		_: return "Rr"

func get_shape_genotype(gene: int) -> String:
	match gene:
		0: return "hh"
		3: return "HH"
		_: return "Hh"

func get_taste_genotype(gene: int) -> String:
	match gene:
		0: return "tt"
		3: return "TT"
		_: return "Tt"

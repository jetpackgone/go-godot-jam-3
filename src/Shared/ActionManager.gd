extends Node

var action
var selected_data: Dictionary = {}
var selected_seed_entry
var pollen := {}
var previewed_seed_data := {}

func set_action(new_action: int):
	if new_action != Constants.ACTION.PLANT:
		selected_data = {}
		if selected_seed_entry != null:
			selected_seed_entry.pressed = false
		selected_seed_entry = null
	if new_action != Constants.ACTION.POLLINATE:
		pollen = {}
	match new_action:
		Constants.ACTION.WATER:
			Input.set_custom_mouse_cursor(load(Constants.CURSOR_WATERING_CAN), 0, Vector2(5, 20))
		Constants.ACTION.PLANT:
			Input.set_custom_mouse_cursor(load(Constants.CURSOR_TROWEL), 0, Vector2(0, 80))
		Constants.ACTION.REMOVE:
			Input.set_custom_mouse_cursor(load(Constants.CURSOR_SCYTHE), 0, Vector2(10, 10))
		Constants.ACTION.POLLINATE:
			if !has_pollen():
				Input.set_custom_mouse_cursor(load(Constants.CURSOR_POLLINATE), 0, Vector2(30, 30))
			else:
				Input.set_custom_mouse_cursor(load(Constants.CURSOR_POLLINATE_BEE), 0, Vector2(30, 30))
		Constants.ACTION.HARVEST:
			Input.set_custom_mouse_cursor(load(Constants.CURSOR_HAND_GRAB), 0, Vector2(0, 24))
		_:
			Input.set_custom_mouse_cursor(load(Constants.CURSOR_HAND_POINT), 0, Vector2(0, 0))
	action = new_action
	EventBus.emit_signal("action_selected", new_action)

func set_seed_to_plant(seed_entry):
	set_action(Constants.ACTION.PLANT)
	selected_seed_entry = seed_entry
	selected_data = seed_entry.data

func remove_selected_seed():
	GameStateManager.remove_seed(selected_data)
	set_action(Constants.ACTION.SELECT)

func has_pollen():
	return pollen != null && !pollen.empty()

func clear():
	selected_data = {}
	selected_seed_entry = null
	pollen = {}
	action = null

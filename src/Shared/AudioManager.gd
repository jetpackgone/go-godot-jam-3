extends Node

var bgm_volume: float = 70
var sfx_volume: float = 25
var ambience_volume: float = 35
var game_sfx_volume: float = 90

func update_bgm_volume(volume: float):
	EventBus.emit_signal("bgm_volume_updated", volume)
	bgm_volume = volume

func update_sfx_volume(volume: float):
	EventBus.emit_signal("sfx_volume_updated", volume)
	sfx_volume = volume

func update_ambience_volume(volume: float):
	EventBus.emit_signal("ambience_volume_updated", volume)
	ambience_volume = volume

func update_game_sfx_volume(volume: float):
	EventBus.emit_signal("game_sfx_volume_updated", volume)
	game_sfx_volume = volume

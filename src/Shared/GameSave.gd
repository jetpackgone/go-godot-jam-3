extends Resource
class_name GameSave

export(String) var version = "1.0"
export(int) var last_save_date = 0
export(int) var created_date = 0

export(Array) var plants = [] # Array of dictionary
export(Array) var seeds = []
export(Array) var fruits = []
export(int) var reputation = 0

export(bool) var intro_done = false
export(int) var tutorial_index = 0

extends PanelContainer

signal confirmed
signal canceled

# Called when the node enters the scene tree for the first time.
func _ready():
	hide()

func _on_CancelButton_pressed():
	emit_signal("canceled")

func _on_OkButton_pressed():
	emit_signal("confirmed")

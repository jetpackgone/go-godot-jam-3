extends Button

func _on_SettingsButton_pressed():
	$ConfirmSfxPlayer.play()
	EventBus.emit_signal("settings_menu_toggled")	

func _on_SettingsButton_mouse_entered():
	$MenuSelectSfxPlayer.play()

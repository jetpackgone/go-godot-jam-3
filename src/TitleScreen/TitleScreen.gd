extends Control

onready var confirmation = $ConfirmationDialog
onready var confirmation_anim_player = $ConfirmationDialog/MenuAnimationPlayer

var load_new_game := true

func _ready():
	$AnimationPlayer.play("RESET")
	EventBus.connect("game_loaded", self, "_on_game_loaded")
	$M/V/ContinueButton.disabled = !SaveSystem.save_exist()
	display()

func _on_game_loaded():
	if load_new_game:
		EventBus.emit_signal("transition_triggered", Constants.PATH_INTRO)
	else:
		EventBus.emit_signal("transition_triggered", Constants.PATH_GAME)

func _on_ContinueButton_pressed():
	load_new_game = false
	SaveSystem.load_game()

func _on_NewButton_pressed():
	if SaveSystem.save_exist():
		confirmation_anim_player.show_menu(confirmation)
	else:
		load_new_game = true
		SaveSystem.load_new_game()

func _on_ExitButton_pressed():
	get_tree().quit()

func _on_ConfirmationDialog_confirmed():
	load_new_game = true
	SaveSystem.load_new_game()

func _on_ConfirmationDialog_canceled():
	confirmation_anim_player.hide_menu(confirmation)

func display():
	$AnimationPlayer.play("display")

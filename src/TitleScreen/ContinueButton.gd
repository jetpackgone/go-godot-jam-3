extends Button


func _on_ContinueButton_mouse_entered():
	if !disabled:
		$MenuSelectSfxPlayer.play()

func _on_ContinueButton_pressed():
	$ConfirmSfxPlayer.play()

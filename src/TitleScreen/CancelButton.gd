extends Button

func _on_CancelButton_mouse_entered():
	$MenuSelectSfxPlayer.play()

func _on_CancelButton_pressed():
	$CancelSfxPlayer.play()

extends Node2D

onready var garden = $Garden
onready var garden_ui = $CanvasLayer/GardenUI
onready var cafe = $Cafe
onready var cafe_ui = $CanvasLayer/CafeUI
onready var cafe_bgm_player = $Cafe/AmbiencePlayer
onready var garden_bgm_player = $Garden/AmbiencePlayer

onready var fader = $CanvasLayer/Fader
onready var fader_player = $CanvasLayer/Fader/AnimationPlayer
onready var fader_timer = $CanvasLayer/Fader/Timer

onready var dialogue_box = $CanvasLayer/DialogueBox

# Called when the node enters the scene tree for the first time.
func _ready():
	garden_bgm_player.fade_in()
	set_mode(Constants.GAME_MODE.GARDEN)
	hide_ui()
	EventBus.connect("game_mode_swapped", self, "_on_game_mode_swapped")
	EventBus.connect("transition_finished", self, "_on_transition_finished")
	EventBus.connect("transition_triggered", self, "_on_transition_triggered")
	EventBus.connect("dev_mode_toggled", self, "_on_dev_mode_toggled")

func set_mode(mode: int):
	GameStateManager.mode = mode
	garden.visible = mode == Constants.GAME_MODE.GARDEN
	garden_ui.visible = mode == Constants.GAME_MODE.GARDEN
	cafe.visible = mode == Constants.GAME_MODE.CAFE
	cafe_ui.visible = mode == Constants.GAME_MODE.CAFE
	ActionManager.set_action(Constants.ACTION.SELECT)
	cafe.toggle_timer(mode == Constants.GAME_MODE.CAFE)
	GameStateManager.set_delete_mode(false)

func _on_game_mode_swapped(new_mode: int):
	if new_mode == Constants.GAME_MODE.CAFE:
		cafe_bgm_player.fade_in()
		garden_bgm_player.fade_out()
	else:
		garden_bgm_player.fade_in()
		cafe_bgm_player.fade_out()
	transition(new_mode)

func _on_transition_finished():
	set_mode(Constants.GAME_MODE.GARDEN)
	if GameStateManager.intro_done:
		return
	dialogue_box.dialog = [
		"Well, here's the garden.",
		"Let's check Grandmother's notes and plant a seed from the inventory."
	]
	dialogue_box.start()
	yield(dialogue_box, "dialogue_finished")
	$CanvasLayer/GardenUI/MarginContainer/Inventory.toggle_menu()
	GameStateManager.intro_done = true
	EventBus.emit_signal("game_save_triggered")

func _on_transition_triggered(_path):
	hide_ui()

func _on_dev_mode_toggled(enabled):
	$CanvasLayer/DevModeLabel.visible = enabled

func hide_ui():
	cafe_ui.hide()
	garden_ui.hide()

func transition(new_mode: int):
	fader_player.play("RESET")
	fader.show()
	fader_player.play("blackout")
	yield(fader_player, "animation_finished")
	
	set_mode(new_mode)
	$DoorSfxPlayer.play()
	fader_timer.start()
	yield(fader_timer, "timeout")
	
	fader_player.play_backwards("blackout")
	yield(fader_player, "animation_finished")
	fader.hide()

func _on_SaveTimer_timeout():
	EventBus.emit_signal("game_save_triggered")

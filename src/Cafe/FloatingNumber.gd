extends Label

var GREEN = '#16621f'
var RED = '#932828'

# Called when the node enters the scene tree for the first time.
func _ready():
	hide()
	$AnimationPlayer.play("RESET")

func display(value: int):
	if value >= 0:
		self.add_color_override("font_color", Color(GREEN))
		text = "+" + str(value)
		show()
		$AnimationPlayer.play("rise")
	else:
		self.add_color_override("font_color", Color(RED))
		text = str(value)
		show()
		$AnimationPlayer.play("sink")


func _on_AnimationPlayer_animation_finished(anim_name):
	self.queue_free()

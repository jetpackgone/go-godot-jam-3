extends Node2D

onready var portrait_left = $PortraitLeft
onready var portrait_right = $PortraitRight
onready var right_anim_player = $PortraitRight/AnimationPlayer
onready var left_anim_player = $PortraitLeft/AnimationPlayer
onready var boba_left = $BobaLeft
onready var boba_right = $BobaRight

func _ready():
	EventBus.connect("gave_boba", self, "_on_gave_boba")

func _on_gave_boba(portrait_index: int, selected_fruit_data: Dictionary):
	var phenotype = GeneHelper.get_phenotypes(selected_fruit_data["genes"])
	var color = phenotype[Constants.GENE_COLOR]
	var shape = phenotype[Constants.GENE_SHAPE]
	var taste = phenotype[Constants.GENE_TASTE]
	
	if portrait_index == 0 && portrait_left.shown:
		GameStateManager.remove_fruit(selected_fruit_data)
		$PourSfxPlayer.play()
		yield($PourSfxPlayer, "finished")
		$SlideSfxPlayer.play()
		boba_left.give_boba(color)
		yield(boba_left, "gave_boba_done")
		portrait_left.update_reaction(color, shape, taste)
		portrait_left.timer.start()
		yield(portrait_left.timer, "timeout")
		portrait_left.hide_portrait()
		boba_left.fade_boba()
	elif portrait_index == 1 && portrait_right.shown:
		GameStateManager.remove_fruit(selected_fruit_data)
		$PourSfxPlayer.play()
		yield($PourSfxPlayer, "finished")
		$SlideSfxPlayer.play()
		boba_right.give_boba(color)
		yield(boba_right, "gave_boba_done")
		portrait_right.update_reaction(color, shape, taste)
		portrait_right.timer.start()
		yield(portrait_right.timer, "timeout")
		portrait_right.hide_portrait()
		boba_right.fade_boba()

func toggle_timer(enabled: bool):
	if enabled:
		$Timer.start()
	else:
		$Timer.stop()

func _on_Timer_timeout():
	generate_customer()

func generate_customer():
	var available_portrait = get_available()
	if available_portrait != null:
		$ShopDoorBellSfxPlayer.play()
		available_portrait.show_random_portrait()

func get_available():
	if !portrait_right.shown:
		return portrait_right
	elif !portrait_left.shown:
		return portrait_left
	return null

extends Control

onready var selected_fruit_preview = $M4/V/PanelContainer/H2/C/SelectedFruit
onready var color_label = $M4/V/PanelContainer/H2/HBoxContainer/Values/ColorLabel
onready var shape_label = $M4/V/PanelContainer/H2/HBoxContainer/Values/ShapeLabel
onready var taste_label = $M4/V/PanelContainer/H2/HBoxContainer/Values/TasteLabel
onready var selection_menu = $M4
onready var selection_menu_anim_player = $M4/MenuAnimationPlayer
onready var tool_tip = $FruitToolTip

var selected_fruit_data

func _ready():
	selection_menu.hide()
	tool_tip.hide()
	EventBus.connect("game_saved", self, "_on_game_saved")
	EventBus.connect("fruit_tool_tip_shown", self, "_on_tool_tip_shown")
	EventBus.connect("fruit_tool_tip_hidden", self, "_on_tool_tip_hidden")
	EventBus.connect("delete_mode_toggled", self, "_on_delete_mode_toggled")

func _on_tool_tip_shown(rect: Rect2):
	tool_tip.refresh()
	tool_tip.rect_position.y = rect.position.y
	tool_tip.show()

func _on_tool_tip_hidden():
	tool_tip.hide()

func _on_GardenButton_pressed():
	EventBus.emit_signal("game_mode_swapped", Constants.GAME_MODE.GARDEN)

func _on_NotesButton_pressed():
	$Notes.toggle_menu()

func _on_SettingsButton_pressed():
	EventBus.emit_signal("settings_menu_toggled")

func _on_game_saved():
	$M2/GameSavedLabel.show()
	$M2/Timer.start()
	yield($M2/Timer, "timeout")
	$M2/GameSavedLabel.hide()

func _on_TestButton_pressed():
	clear_selection()
	EventBus.emit_signal("gave_boba", 0, selected_fruit_data)
	selection_menu_anim_player.hide_menu(selection_menu)

func _on_TestButton2_pressed():
	clear_selection()
	EventBus.emit_signal("gave_boba", 1, selected_fruit_data)
	selection_menu_anim_player.hide_menu(selection_menu)

func _on_Fruit_fruit_selected(entry):
	if GameStateManager.delete_mode:
		return
	selected_fruit_data = entry.data
	selected_fruit_preview.texture = entry.texture_rect.texture
	var phenotype = GeneHelper.get_phenotypes(entry.data["genes"])
	color_label.text = phenotype[Constants.GENE_COLOR]
	shape_label.text = phenotype[Constants.GENE_SHAPE]
	taste_label.text = phenotype[Constants.GENE_TASTE]
	if !selection_menu.visible:
		selection_menu_anim_player.show_menu(selection_menu)

func clear_selection():
	selected_fruit_preview.texture = null
	shape_label.text = ""
	color_label.text = ""
	taste_label.text = ""

func set_delete_mode(is_delete_mode):
	GameStateManager.set_delete_mode(is_delete_mode)
	ActionManager.set_action(Constants.ACTION.SELECT)

func _on_DiscardButton_toggled(button_pressed):
	set_delete_mode(button_pressed)

func _on_delete_mode_toggled(toggled):
	$M5/VBoxContainer/DiscardButton.pressed = toggled

extends TextureRect

signal gave_boba_done

onready var anim_player = $AnimationPlayer

var RED_BOBA = "res://Cafe/red_boba.tres"
var YELLOW_BOBA = "res://Cafe/yellow_boba.tres"

func _ready():
	anim_player.play("RESET")

func set_boba_color(color: String):
	if color == "red":
		texture = load(RED_BOBA)
	else:
		texture = load(YELLOW_BOBA)

func give_boba(color: String):
	anim_player.play("RESET")
	set_boba_color(color)
	anim_player.play("give")
	yield(anim_player, "animation_finished")
	emit_signal("gave_boba_done")

func fade_boba():
	anim_player.play("fade")
	yield(anim_player, "animation_finished")

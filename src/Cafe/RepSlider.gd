extends VBoxContainer

onready var tween = $RepSlider/Tween
onready var sparkles = $RepSlider/SparkleParticles

func _ready():
	$RepSlider.value = round(GameStateManager.reputation)
	$M/Label.text = str(round(GameStateManager.reputation))
	EventBus.connect("reputation_updated", self, "_on_reputation_updated")

func _on_reputation_updated(new_rep: int):
	set_value(new_rep)

func set_value(new_value: float):
	$M/Label.text = str(round(new_value))
	move_slider(round(new_value))
	sparkles.emitting = new_value >= 100

func move_slider(new_value: float):
	if tween.is_active():
		tween.stop($RepSlider, "value")
	var duration = abs($RepSlider.value - new_value) / 20 * 0.25
	tween.interpolate_property($RepSlider, "value", $RepSlider.value, new_value, duration, Tween.TRANS_CUBIC, Tween.EASE_IN_OUT)
	tween.start()

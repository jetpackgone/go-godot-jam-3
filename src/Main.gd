extends Node2D

onready var fader = $Canvas/Fader
onready var fader_player = $Canvas/Fader/AnimationPlayer
onready var fader_timer = $Canvas/Fader/Timer

var current_scene: Node

func _ready():
	EventBus.connect("transition_triggered", self, "_on_transition_triggered")
	current_scene = $TitleScreen
	Input.set_custom_mouse_cursor(load(Constants.CURSOR_HAND_POINT), 2, Vector2(0, 0))
	Input.set_custom_mouse_cursor(load(Constants.CURSOR_HAND_GRAB), 6, Vector2(0, 0))
	fader_player.play_backwards("blackout")
	yield(fader_player, "animation_finished")
	fader.hide()
	$BgmPlayer.play()

func _on_transition_triggered(new_scene_path: String):
	fader_player.play("RESET")
	fader.show()
	fader_player.play("blackout")
	yield(fader_player, "animation_finished")
	
	remove_child(current_scene)
	var new_scene = load(new_scene_path).instance()
	add_child(new_scene)
	current_scene.queue_free()
	current_scene = new_scene
	fader_timer.start()
	yield(fader_timer, "timeout")
	
	fader_player.play_backwards("blackout")
	yield(fader_player, "animation_finished")
	fader.hide()
	EventBus.emit_signal("transition_finished")

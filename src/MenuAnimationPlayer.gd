extends AnimationPlayer

func show_menu(menu: Control):
	play("RESET")
	menu.show()
	play("show")

func hide_menu(menu: Control):
	play("hide")
	yield(self, "animation_finished")
	menu.hide()

func toggle_menu(menu: Control):
	if !is_playing():
		if menu.visible:
			hide_menu(menu)
		else:
			show_menu(menu)
